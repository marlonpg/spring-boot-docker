package com.gambasoftware.springbootdocker.boundaries.driving.controllers;

import com.gambasoftware.springbootdocker.application.ports.ShoppingListPort;
import com.gambasoftware.springbootdocker.boundaries.driven.database.models.ShoppingListModel;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("shopping-list")
public class ShoppingListController {

    private ShoppingListPort shoppingListPort;

    public ShoppingListController(ShoppingListPort shoppingListPort) {
        this.shoppingListPort = shoppingListPort;
    }

    @PostMapping
    public Mono<ShoppingListModel> create(@RequestBody ShoppingListModel shoppingListModel) {
        return shoppingListPort.create(shoppingListModel);
    }

    @GetMapping("{id}")
    public Mono<ShoppingListModel> get(@PathVariable String id) {
        return shoppingListPort.get(id);
    }
}
