package com.gambasoftware.springbootdocker.boundaries.driven.database.models;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ShoppingListRepository extends ReactiveMongoRepository<ShoppingListModel, String> {
}
