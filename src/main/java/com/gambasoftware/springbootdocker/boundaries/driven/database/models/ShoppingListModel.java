package com.gambasoftware.springbootdocker.boundaries.driven.database.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "shoppingLists")
public class ShoppingListModel {
    @Id
    private String id;
    private String description;
    private LocalDateTime createdAt;

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "ShoppingListModel{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }

    public static final class ShoppingListModelBuilder {
        private String id;
        private String description;
        private LocalDateTime createdAt;

        private ShoppingListModelBuilder() {
        }

        public static ShoppingListModelBuilder aShoppingListModel() {
            return new ShoppingListModelBuilder();
        }

        public ShoppingListModelBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public ShoppingListModelBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public ShoppingListModelBuilder withCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public ShoppingListModel build() {
            ShoppingListModel shoppingListModel = new ShoppingListModel();
            shoppingListModel.createdAt = this.createdAt;
            shoppingListModel.id = this.id;
            shoppingListModel.description = this.description;
            return shoppingListModel;
        }
    }
}
