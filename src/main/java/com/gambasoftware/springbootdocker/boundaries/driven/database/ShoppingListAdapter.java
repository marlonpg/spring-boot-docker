package com.gambasoftware.springbootdocker.boundaries.driven.database;

import com.gambasoftware.springbootdocker.application.ports.ShoppingListPort;
import com.gambasoftware.springbootdocker.boundaries.driven.database.models.ShoppingListModel;
import com.gambasoftware.springbootdocker.boundaries.driven.database.models.ShoppingListRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class ShoppingListAdapter implements ShoppingListPort {

    private ShoppingListRepository shoppingListRepository;

    public ShoppingListAdapter(ShoppingListRepository shoppingListRepository) {
        this.shoppingListRepository = shoppingListRepository;
    }

    public Mono<ShoppingListModel> create(ShoppingListModel shoppingListModel) {
        return shoppingListRepository.save(shoppingListModel);
    }

    @Override
    public Mono<ShoppingListModel> get(String id) {
        return shoppingListRepository.findById(id);
    }
}
