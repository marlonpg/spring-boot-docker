package com.gambasoftware.springbootdocker.application.ports;

import com.gambasoftware.springbootdocker.boundaries.driven.database.models.ShoppingListModel;
import reactor.core.publisher.Mono;

public interface ShoppingListPort {
    Mono<ShoppingListModel> create(ShoppingListModel shoppingListModel);
    Mono<ShoppingListModel> get(String id);
}
